/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha6_lp1;

import java.util.Scanner;

/**
 *
 * @author Andre Maia
 */
public class Ficha6_LP1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //variaveis
        double salario = 0;
        int idade = 0;
        Scanner ler = new Scanner(System.in);
        
        double tsu_Empresa =0;
        double tsu_Funcionario =0;
        
        
        //Leitura Variaveiss
        System.out.println("-- Calculo Contribuições para a Segurança Social --");
        System.out.println("Introduza o seu salario:");
        salario = ler.nextDouble();
        System.out.println("Introduza a sua idade:");
        idade = ler.nextInt();
        
        

        //Logica do programa
        
        if (idade < 55) {
        
            tsu_Empresa = salario * 0.20;
            tsu_Funcionario = salario * 0.17;
            
        }
        
        if (idade >= 55 && idade <= 65) {
              
            tsu_Empresa = salario * 0.13;
            tsu_Funcionario = salario * 0.13;
            
        }
        
        
        if (idade > 65) {
            
            tsu_Empresa = salario * 0.5;
            tsu_Funcionario = salario * 0.075;
        }
        
        
        System.out.println("Resultado:");
        System.out.print("Para um salario de :" + salario);
        System.out.print(" e uma idade de " + idade);
        System.out.println(" temos o seguinte resultado:");
        System.out.println("TSU EMPRESA: " + tsu_Empresa);
        System.out.println("TSU FUNCIONÁRIO: " + tsu_Funcionario);
        
    }
    
}
